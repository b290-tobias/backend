// console.log("Kabahan ka na");

// [ S E C T I O N ] Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time
// pagnag error na sa again, di na nagrurun si bye


console.log("Hello");
// conosle.log("Again");
console.log("Bye");


for(let i = 0; i <= 10; i++) {
	console.log(i);
};

console.log("Henlo~");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background
















// [ S E C T I O N ] Getting all posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

/*
	Syntax:
		fetch("URL")
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));










// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
// The "then" method captures the "Response" object and returns another "promise" which will eventually be "resolved" or "rejected"


fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => console.log(response.status));




// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
// Print the converted JSON value from the "fetch" request
// Using multiple "then" methods creates a "promise chain"

fetch("https://jsonplaceholder.typicode.com/posts")
	.then((response) => response.json())
	.then((json) => console.log(json));










// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for



// creates an asynchronous function
async function fetchData() {
	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	//Result returned by fetch returns a promise
	console.log(result);
	console.log(typeof result);
	console.log(result.body);


	let json = await result.json();
	console.log(json);
};

fetchData();


















// [ S E C T I O N ] Getting a specific post
// Retrieves a specific post following the REST API
// (/posts/:id, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(res => res.json())
	.then(json => console.log(json));







// [ S E C T I O N ] Creating a post
/*
	Syntax:
		fetch("URL", options)
			.then(res => {})
			.then(res => {})

*/


// Create a new post following the REST API ( /posts/:userId)
fetch("https://jsonplaceholder.typicode.com/posts", 
	{
		// Sets the method of the "Request" object to "POST" following REST API
		// Default method is GET
		method : "POST",
		// Sets the header data of the "Request" object to be sent to the backend
		// Specified that the content will be in a JSON structure
		headers : {
			"Content-Type" : "application/json"
		},
		// Sets the content/body data of the "Request" object to be sent to the backend
		// JSON.stringify converts the object data into a stringified JSON
		body : JSON.stringify ( {
			title : "New post",
			body : "Hello World",
			userId : 1
		} )
	}
)
	.then(response => response.json())
	.then(json => console.log(json));








// [ S E C T I O N ] Updating a post

// /posts/:id

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "PUT",
	headers : {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify ({
		id : 1,
		title : "Updated post",
		body : "Hello again"
	})
})
	.then(response => response.json())
	.then(json => console.log(json));






// [ S E C T I O N ] Updating a post using PATCH Method

// /posts/:id

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "PATCH",
	headers : {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify ({
		title : "Updated post using PATCH",
	})
})
	.then(response => response.json())
	.then(json => console.log(json));







// [ S E C T I O N ] Deleting a post

// /posts/:id

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "DELETE"})
	.then(response => response.json())
	.then(json => console.log(json));









// [ S E C T I O N ] Filtering Posts
// The data can be filtered by sending the userId along with the URL
// Information sent vie the url can be done by adding the question mark symbol (?)

/*
	Syntax:
		Individual Parameter
			"url?parameterName=value"
		Multiple Parameter
			"url?paramA=value&paramB=valueB"
*/


fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
	.then(response => response.json())
	.then(json => console.log(json));





// [ S E C T I O N ] Retrieving nester/related comments to posts
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
	.then(response => response.json())
	.then(json => console.log(json));
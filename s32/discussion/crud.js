const http = require("http");

const port = 4000;

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Brandy",
		"email" : "brandy@mail.com"
	}
];

const app = http.createServer(function (request, response) 
	{
		if (request.url === "/users" && request.method === "GET") 
		{
			response.writeHead(200, { "Content-Type" : "application/json" });
			response.write(JSON.stringify(directory));
			response.end();
		};

		if (request.url === "/users" && request.method === "POST") 
		{
			let requestBody = "";
			request.on("data", function(data){
				requestBody += data;
			});

			request.on("end", function(){
				console.log(typeof requestBody);
				requestBody = JSON.parse(requestBody);
				let newUser = {
					"name" : requestBody.name,
					"email" : requestBody.email
				};
				directory.push(newUser);
				console.log(directory);
				response.writeHead(200, { "Content-Type" : "application/json"});
				response.write(JSON.stringify(newUser));
				response.end();
			});
		}

	}
);

app.listen(port, () => console.log(`Server running at localhost:${port}.`));



// response.writeHead(200, { "Content-Type" : "application/json" })
// Requests the "/users" path and "GETS" information 
// Sets the status code to 200, denoting OK
// Sets response output to JSON data type


// 			response.write(JSON.stringify(directory));
// Input HAS to be data type STRING hence the JSON.stringify() method
// This string input will be converted to desired output data type which has been set to JSON
// This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON


// let requestBody = "";
// Declare and intialize a "requestBody" variable to an empty string
// This will act as a placeholder for the resource/data to be created later on


// let requestBody = "";
// place holder only


// request.on("data", function(data){
// A stream is a sequence of data
// Data is received from the client and is processed in the "data" stream
// The information provided from the request object enters a sequence called "data" the code below will be triggered
// data step - this reads the "data" stream and processes it as the request body
//request.on is an event trigger


// requestBody += data;
// Assigns the data retrieved from the data stream to requestBody

// console.log(typeof requestBody);
// Check if at this point the requestBody is of data type STRING
// We need this to be of data type JSON to access its properties


//requestBody = JSON.parse(requestBody);
// Convert JSON to javascript object


/*
				let newUser = {
					"name" : requestBody.name,
					"email" : requestBody.email
				};
*/
// parse the data


// directory.push(newUser);
// add the new user into the moch database


// What is a Stream
		https://study.com/academy/lesson/streams-in-computer-programming-definition-examples.html


// Node JS Stream Documentation
		https://nodejs.org/api/stream.html#stream_event_data
//Add code here



const http = require("http");

const port = 4000;

const app = http.createServer((req, res) => {
        if (req.url == "/" && req.method == "GET") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Welcome to Booking System");
        } else if (req.url == "/profile" && req.method == "GET") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Welcome to your profile!");
        } else if (req.url == "/courses" && req.method == "GET") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Here's our courses available");
        } else if (req.url == "/addCourse" && req.method == "POST") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Add a course to our resources");
        } else if (req.url == "/updateCourse" && req.method == "PUT") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Update a course to our resources");
        } else if (req.url == "/archiveCourses" && req.method == "DELETE") {
            res.writeHead(200, {"Content-Type" : "text/plain"});
            res.end("Archive courses to our resources");
        }
    }
);

// app.listen(port);

console.log(`Server now running at localhost:${port}.`);
 





//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
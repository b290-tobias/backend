const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");


const app = express();

// Allows access to routes defined within our application
// created after userRoute file has been created
const userRoute = require("./routes/userRoute.js");

// added since we created courseRoute.js
// add app.user after at the bottom
const courseRoute = require("./routes/courseRoute.js");

// Connect MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@zuitt.ziq0mma.mongodb.net/booking-DB?retryWrites=true&w=majority", {
		useNewUrlParser : true,
		useUnifiedTopology : true
});


mongoose.connection.once("open", () => console.log(`Now connected to MongoDB Atlas.`));




// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended : true }));

// Allows all resources to access our backend application
app.use(cors());


// Routes

// Defines the "/users" string to be included for all user routes defined in the "user" route file
// created after userRoute file has been created
app.use("/users", userRoute);

// Defines the "/courses" string to be included for all course routes defined in the "course" route file
// added since we created courseRoute.js
app.use("/courses", courseRoute);

if(require.main === module) {
	app.listen(process.env.PORT || 4000, () => {
		console.log(`API is now online on port ${process.env.PORT || 4000}`)
	});
};


module.exports = { app, mongoose };

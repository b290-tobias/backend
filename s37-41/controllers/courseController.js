const Course = require("../models/Course.js")





// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

// MINI ACTIVITY
	// Create/Instantiate a nerw object for new Course
// MINI ACTIVITY
	// Create/Instantiate a nerw object for new Course
	// Stretch goals: return the saved new Course

module.exports.addCourse = reqBody => {

	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newCourse.save()
		.then(course => true)
		.catch(err => false);
};


// solution #2
/*
module.exports.addCourse = data => {
	
	if (data.iasAdmin) {
	
		let newCourse = new Course ({
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price
			});
	} else {

		return newCourse.save()
			.then(course => true)
			.catch(err => false);
	} else {
	
		console.log ({ auth :  "unathorized user"})

		return false

	}
}

*/



// Retrieve all courses


module.exports.getAllCourses = () => {

	return Course.find({})
		.then(result => {
			return result;
		}).catch(err => {
			console.log(err);
			return false;
		});
};




// Retrieve all ACTIVE courses


module.exports.getAllActiveCourses = () => {

	return Course.find({ isActive : true})
		.then(result => {
			return result;
		}).catch(err => {
			console.log(err);
			return false;
		});
};


// Retrieve a specific course
// reqParams was used since the parameter is not comming from the Body
// Retrieving a specific course
		/*
			Steps:
			1. Retrieve the course that matches the course ID provided from the URL
		*/

module.exports.getCourse = reqParams => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	}).catch(err => {
		console.log(err);
		return false;
	});
};






// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	// Syntax
	// findByIdAndUpdate(document ID, updatesToBeApplied)
	// courseID is based on router..put :/courseId as per courseRoute.js
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
		.then(course => {
			return true;
		}).catch(err => {
			console.log(err);
			return false
		});
};



// Archiving a course


module.exports.archiveCourse = (reqParams, reqBody) => {

	return Course.findByIdAndUpdate(reqParams.courseId, {isActive : reqBody.isActive})
		.then(course => {
			return true;
		}).catch(err => {
			return false
		});
};
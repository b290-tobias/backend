// dito nilagay since yung module nandito and para dito password : reqBody.password

const bcrypt = require("bcrypt");




const auth = require ("../auth.js");

// since we added courseId in the user route and user controller.js
const Course = require("../models/Course.js")


// no need to declare the below as this is already declared in the model
// const mongoose = require("mongoose");
// only declare the below


// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require ("../models/User.js");





// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/


// connected to the below in the userROute.js
/*
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController));
});
*/
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email : reqBody.email })
		.then(result => {
			
			if (result.length > 0) {
				
				return true;
			} else {
				
				return false;
			}
		}).catch(err => {
			console.log(err)
		});
};






// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = reqBody => {
	
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		// https://www.tutorialspoint.com/hashing-passwords-in-python-with-bcrypt#:~:text=rounds%20%E2%88%92%20The%20number%20of%20rounds,value%20between%2010%20and%2015.
		password : bcrypt.hashSync(reqBody.password, 10)

	});


	// Save the created object to our database
	return newUser.save()
		.then(user => true)
		.catch(err => false);


};








// User authentication (login)
// ensure to add this on the above - const auth = require ("../auth");
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = reqBody => {

	return User.findOne({ email : reqBody.email})
		.then(result => {

			// User does not exist
			if (result == null) {

				return false; //user is not registered

			// User exist
			} else {

				// check if password is the same with the compareSync method
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				// passwords match
				if (isPasswordCorrect) {

					return { access : auth.createAccessToken(result)}
				} else {

					console.log("Password did not match")
					return false; //password did not match
				}

			}
		}).catch(err => {
			console.log(err)
		});
};






// activity s38 User details

/*module.exports.userDetails = reqBody => {

	return User.findOne({ _id : reqBody._id})
		.then(result => {
			return {
				_id : result._id,
				firstName : result.firstName,
				lastName : result.lastName,
				email : result.email,
				password : "",
				isAdmin : result.isAdmin,
				mobileNo : result.mobileNo,
				enrollments : result.enrollments,
				__v : result.__v
			}

		}).catch(err => {
			console.log(err)
		})
}*/



// return User.finbyId() => 
// result.password == "";
// return result


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	}).catch(err => {

		console.log(err);

		return false
	});

};



// Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
// data ang gamit since this was estblished in userRoute.js
// user is the same as result sa iba

module.exports.enroll = async (userId, reqBody) => {

	// Add the course ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(userId)
		.then(user => {

			console.log("This is the user");
			console.log(user);

			// Adds the courseId in the user's enrollments array
			// galing sa mongo db yung user.enrollments
			user.enrollments.push({ courseId : reqBody.courseId });

			// Saves the updated user information in the database
			return user.save()
				.then(user => true)
				.catch(err => {
					console.log(err);
					return false
				})

		}).catch(err => {
			console.log(err);
			console.log("error in retrieving user");
			return false
		});


	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	// Adds the userId in the course's enrollees array
	//userid and courseId lang kelangan iupdate since merong dfault yung ibang fields
	let isCourseUpdated = await Course.findById(reqBody.courseId)
		.then(course => {

			course.enrollees.push({ userId : userId });

			return course.save()
				.then(course => true)
				.catch(err => {
					console.log(err);
					return false
				})
		}).catch(err => {
			console.log(err);
			console.log("error in retrieving course");
			return false
		});


	// ternary operation instead of if else
	return (isUserUpdated && isCourseUpdated) ? true : false;

	/* if else equivalent

	if (isUserUpdated && isCourseUpdated) {
		console.log("successful")
		return true;
	} else {
		console.log("unsuccessful")
		return false;
	}

	*/

};


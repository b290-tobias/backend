const express = require("express");
const router = express.Router();

const auth = require("../auth");
const courseController = require("../controllers/courseController.js");




// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	if (userData.isAdmin == true) {

		courseController.addCourse(req.body)
			.then(resultFromController => res.send(resultFromController));
	} else {

		res.send("User is not Admin")
	}
});



// Solution #2


/*
router.post("/", auth.verify, (req, res) => {

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data)
			.then(resultFromController => res.send(resultFromController));

})

*/








// Route for retrieving all courses

router.get("/all", auth.verify, (req, res) => { //need for middleware

	const userData = auth.decode(req.headers.authorization);

		console.log(userData);

		if (userData.isAdmin == true) {

			courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

		} else {

			res.send("User is not Admin")
		}

	
});



// Route for retrieving all the ACTIVE courses
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the courses
router.get("/", (req, res) => {

	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController));
});





// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url

//// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route


router.get("/:courseId", (req, res) => {

	console.log(req.params);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});










// Route for updating a course
// JWT verification is needed for this route to ensure that a user is logged in before updating a course

router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

		console.log(userData);

		if (userData.isAdmin == true) {

			// 2 were used req.params and req.body since we will access both
			courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

		} else {

			console.log({auth : "unauthorized user"});
			res.send("User is not Admin")
		}
	
});







// Route for archiving course

router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

		console.log(userData);

		if (userData.isAdmin == true) {

			// 2 were used req.params and req.body since we will access both
			courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

		} else {

			console.log({auth : "unauthorized user"});
			res.send("User is not Admin")
		}
	
});

module.exports = router;

const express = require("express");
const router = express.Router();


const auth = require("../auth");
const userController = require("../controllers/userController.js");




// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
// should also declare the below in the above
// const userController = require("../controllers/userController.js");
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController));
});







// router for user regisration

router.post("/register", (req, res) => {
	userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController));
});






// router for login

router.post("/login", (req, res) => {
	userController.loginUser(req.body)
		.then(resultFromController => res.send(resultFromController));
});


// activity s38 router for details

/*router.post("/details", (req, res) => {
	userController.userDetails(req.body)
		.then(resultFromController => res.send(resultFromController));
});*/




// added auth.verify because of the verification made in auth.js
// add also the cons above
// // The "auth.verify" acts as a middleware to ensure that the user is logged in before they can access user details
router.post("/details", auth.verify, (req, res) => {


	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id})
		.then(resultFromController => res.send(resultFromController));
});










// Route to enroll user to a course
router.post("/enroll", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	let userId = userData.id

	console.log("This is userID")
	console.log(userId)

/*	let data = {
		courseId : req.body.courseId
	};*/

/*	userController.enroll(req.body)
		.then(resultFromController => res.send(resultFromController));*/
	// use data instead since we create let data

/*	userController.enroll(data)
		.then(resultFromController => res.send(resultFromController));*/

	userController.enroll(userId, req.body)
		.then(resultFromController => res.send(resultFromController));	

});








// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;



// after the above add the below in index.js
// const userRoute = require("./routes/userRoute.js")
// app.use("/users", userRoute);
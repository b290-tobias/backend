// use the require directive to load Node.js

// hypertext transfer protocal
/* An overview of HTTP
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview*/

// A "pakacge" or "module" is a sofawre compenent or part of a program that contains one or more routine

// The "http module" lets node.js transfer data using Hyper Text Transfer Protocol

// Clients (browser) and servers (node/express) communicate by exchanging individual messages



let http = require("http");

// Using this module "createServer()" method, we can create an HTTP SERVER that listens on a specified port.

// http.createServer( function (request, response) {} ).listen(4000);

// if 4000 is occupied you can use 4001 or so on and so forth. commonly 3000 and up ang pwede

// A port is a virtual point where network connection start and end.

// Each port is associate with specific process or service

// https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers


http.createServer( function (request, response) {
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Hello World!")
	} 
).listen(4000);

console.log("Server running at localhost:4000");

	//response.writeHead(200, {"Content-Type" : "text/plain"});
	// writeHead() method - set the status code for the response - 200 -> OK/SUCCESS
	// https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	// https://www.restapitutorial.com/httpstatuscodes.html

	// response.end("Hello World!")
	// send the response with text content "Hello World!"

	// console.log("Server running at localhost:4000");
	// Whenever the server starts, console will print the message in our "terminal".
	// kasi backend server na siya di siya magdisplay sa browser console since naka frontend siya
	
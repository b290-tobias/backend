// CRUD Operations

// [ S E C T I O N ] Inserting Documents (Create)
/*
Syntax: 
db.collectionName.insertOne({
	object
})
*/

// Inserting a single document
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});








// Inserting multiple documents
/*
Syntax: 
db.collectionName.insertMany([
	{object A},
	{object B}
])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "09123456789",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "09123456789",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
]);










// [ S E C T I O N ] finding documents (Read)

// syntax: db.collectionName.find();
// syntax: db.collectionName.find({field: value});

//  syntax: db.collectionName.findOne({field: value});
// if multiple results the 1st one on the list

//Leaving the search criteria emply will retrieve all the documents
db.users.find();

db.users.find({ firstName: "Stephen"});



// Finding documents with multiple parameters
// Syntax: db.collectionName.Name.find({fieldA: valueA, field B: valueB})
db.users.find({ lastName: "Armstrong", age: 82});






// [ S E C T I O N ] Updating documents (Update)
//Updating a single document

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});


//Syntax : db.collectionName.updateOne({criteria}, {$set: {field: value}});
// updateOne will only update the first document that matches the search criteria

db.users.updateOne(
	{ firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);


db.users.find({ firstName: "Bill"});




// Updateing multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set: {fieldA: value}});

*/

db.users.updateMany(
	{ department: "none"},
	{
		$set: {department: "HR"}
	}
);





// Replace One 
// replaces the whole document

db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "09123456789",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
	}
);


db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
	}
);



db.hotel.replaceOne(
	{ name: "single"},
	{
		name: "single",
		accomodates: 2,
		price: 1000,
		description: "A simple room with all the basic necessities",
		room_available: 10,
		isAvailable: false
	}
);









// [ S E C T I O N ] Deleting documents (Delete)

// Document to be deleted
db.users.insertOne ({
	firstName: "test"
});


// Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "test"});

// DeletingMany
/*
	Syntax:
		db.collectionName.deleteMany({criteria})
*/
db.users.deleteMany({firstName: "Bill"});



[ S E C T I O N ] Advance queries

db.users.find({
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	}
});


db.users.find({"contact.email" : "janedoe@gmail.com"});


//querying an array should be in order
db.users.find({courses: ["CSS", "JavaScript", "Python"]});

//will not work
db.users.find({courses: ["CSS", "Python", "JavaScript"]});


// querying an array without regard to order
db.users.find({courses: {$all: ["React", "Python"]}});


// querying an embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}

	]
});

db.users.find({
	namearr: {
		namea: "juan"
	}
});


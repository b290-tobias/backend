// Use the "require" directive to load the express module/package
// A "module" is a software component or part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to easily create a server

const express = require("express");


// Create an application using express
// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express();

//port to listed
const port = 4000;


// Setup for allowing the server to handle data from requests
// Allows your app to read json data
// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
// API management is one of the common application of middlewares.
// app (server) is galing sa const app then use method to access express then json method/gagawing json
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({ extended : true }));


// [ S E C T I O N ] Routes
// Express has methods corresponding to each HTTP method
// This route expects to receive a GET request at the base URI "/"
// The full base URI for our local application for this route will be at "http://localhost:4000"
// This route will return a simple message back to the client

app.get("/", (req, res) => {

	// Once the route is accessed it will in send a string response containing "Hello World"
    // Compared to the previous session, res.end uses the node JS module's method
    // res.send uses the express JS module's method instead to send a response back to the client
	res.send("Hello B290! Welcome to Express.");

});


// This route expects to receive a GET request at the URI "/hello"
app.get("/hello", (req, res) => {
	res.send("Hello from /hello endpoint");

});


// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);

});



// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database
let users = [];
app.post("/signup", (req, res) => {

	// If contents of the "request body" with the property "username" and "password" is not empty
	if (req.body.username !== "" && req.body.password !== "") {
		// This will store the user object sent via Postman to the users array created above
		users.push(req.body)

		console.log(users)

		res.send(`User ${req.body.username} succesfully registered!`);
	} else {
		res.send("Please input BOTH usename and password.")
	}
});



// This route expects to receive a PUT request at the URI "/change-password"
// This will update the password of a user that matches the information provided in the client/Postman
app.put("/change-password", (req, res) => {

	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++) {

		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(req.body.username == users[i].username) {

			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = req.body.password;

			console.log(users[i]);

			// Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`;

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user was found
		} else {

			
			// Changes the message to be sent back by the response
			message = "User does not exist.";
		}
	};


	// Sends a response back to the client/Postman once the password has been updated or if a user is not found
	res.send(message);
});





// Activity

//1-2
app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});


//3-4
users = [{
	"username" : "johndoe",
	"password" : "johndoe1234"
}];
app.get("/users", (req, res) => {
	res.send(users);
});

//5-6

app.delete("/delete-user", (req, res) => {
	let messageDelete;

	for(let i = 0; i < users.length; i++) {
		if(req.body.username == users[i].username) {
			users.splice(users.indexOf(users[i].username),1);
			messageDelete = `User ${req.body.username} has been deleted.`;
			break;
		}

		if (messageDelete == undefined) {
			messageDelete = `User does not exist`;
		} else {
			messageDelete = `No users found`;
		}
	};

	

	res.send(messageDelete);
});


//if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly.
//else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
if(require.main === module) {
	// Tells our server to listen to the port
	// If the port is accessed, we can run the server
	// Returns a message to confirm that the server is running in the terminal
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;



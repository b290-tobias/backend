// console.log("Hello. Ang cute ko.")

// [ S E C T I O N ] ARITHMETIC OPERATORS

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	// MINI ACTIVITY

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);
	
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// [S E C T I O N ] Assignment Operators

	// Basic Assignment Operator (=)
	// The assignment operator assigns the value of the **right** operand to a variable.

	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition assigment operator " + assignmentNumber);

	// Shorthand for assignment

	assignmentNumber += 2;
	console.log("Result of addition assignment operator " + assignmentNumber);

	// MINI ACTIVITY
	/*
		have subtraction, multiplication, division assignment operators
	*/

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator " + assignmentNumber);

	assignmentNumber %= 2;
	console.log("Result of modulo assignment operator " + assignmentNumber);



	// Multiple Operators and Parenthesis
	/*
	- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
    - The operations were done in the following order:

				   1. 3 * 4 = 12
	               2. 12 / 5 = 2.4
	               3. 1 + 2 = 3
	               4. 3 - 2.4 = 0.6

	*/
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas);


	/*
        - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
        - The operations were done in the following order:
            1. 4 / 5 = 0.8
            2. 2 - 3 = -1
            3. -1 * 0.8 = -0.8
            4. 1 + -.08 = .2
    */


	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of mdas operation: " + pemdas);



	pemdas = (1 + (2 - 3)) * (4 / 5);
	console.log("Result of mdas operation: " + pemdas);



	// Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;

	let increment = ++z;
	// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
	console.log("Result of pre increment: " + increment);

	// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
	console.log("Result of pre increment: " + z);


	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
	
	increment = z++;
	console.log("Result of post increment: " + increment);


	// The value of "z" is at 2 before it was incremented
	console.log("Result of post increment: " + z);

	

	let decrement = --z;

	// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "decrement"

	// The value of "z" is at 3 before it was decremented
	console.log("Result of pre decrement: " + decrement);

	// The value of "z" was decreased reassigning the value to 2
	console.log("Result of pre decrement: " + z);



	// The value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one
	decrement = z--;
	
	// The value of "z" is at 2 before it was decremented
	console.log("Result of post decrement: " + decrement);
	
	// The value of "z" was decreased reassigning the value to 1
	console.log("Result of post decrement: " + z);

	//save muna yung value sa decrement kaya pag ni call yung decrement same ng value ng z
	//yung z is after na maconclude yung operation


// [S E C T I O N ] COMPARISON OPERATORS
	let juan = "juan";

	// (Loose) Equality Operator
	/* 
        - Checks whether the operands are equal/have the same content
        - Attempts to CONVERT AND COMPARE operands of different data types
        - Returns a boolean value
    */


	console.log(1 == 1); //true
	console.log(1 == 2); // false
	console.log(1 == "1"); //true
	console.log(0 == false); //true since in s 0 is false and 1 is true
	console.log("juan" == "juan"); //true
	console.log("juan" == juan); //true


	// Strict Equality Operator 
	/* 
        - Checks whether the operands are equal/have the same content
        - Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
        - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
        - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
        - Strict equality operators are better to use in most cases to ensure that data types provided are correct
    */


 	console.log(1 === 1); //true
	console.log(1 === 2); // false
	console.log(1 === "1"); //false same value but different data type
	console.log(0 === false); //false since in s 0 is false and 1 is true however different data types
	console.log("juan" === "juan"); //true
	console.log("juan" === juan); //true


	// (Loose) Inequiality Operator
	/* 
        - Checks whether the operands are not equal/have different content
        - Attempts to CONVERT AND COMPARE operands of different data types
    */

    console.log(1 != 1); //false
	console.log(1 != 2); // true
	console.log(1 != "1"); //false same value but different data type
	console.log(0 != false); //false since in s 0 is false and 1 is true however different data types
	console.log("juan" != "juan"); //false
	console.log("juan" != juan); //false


	// Strcit Inequality Operator

	/* 
	    - Checks whether the operands are not equal/have the same content
	    - Also COMPARES the data types of 2 values
	*/

	console.log(1 !== 1); //false
	console.log(1 !== 2); // true
	console.log(1 !== "1"); //true same value but different data type
	console.log(0 !== false); //true since in s 0 is false and 1 is true however different data types
	console.log("juan" !== "juan"); //false
	console.log("juan" !== juan); //false


	// [ S E C T I O N] RELATIONAL OPERATORS
	//Some comparison operators check whether one value is greater or less than to the other value.
	let a = 50;
	let b = 65;

	// GT or Greater Than operator (>)
	let isGreaterThan = a > b;
	// LT or Less Than operator (<)
	let isLessThan = a < b;
	// GTE or Greater Than or Equal operator (>=)
	let isGTorEqual = a >= b;
	// LTE or Less Than or Equal operator (<=)
	let isLTorEqual = a <= b;

	console.log(isGreaterThan); //false
	console.log(isLessThan); //true
	console.log(isGTorEqual); //false
	console.log(isLTorEqual); //true


	let numStr = "30";
	console.log(a > numStr); //true - force coersion - converted string to a number
	console.log(b <= numStr); //false

	let str = "twenty";
	console.log(b >= str); // false - str resulted to NaN since string is not numeric

	//ascii code - https://www.vlsifacts.com/wp-content/uploads/2023/02/ASCII-Code.png
	//iba iba ang values - apple, Apple, APPle, appLE



// [ S E C T I O N ] LOGICAL OPERATORS
	let isLegalAge = true;
	let isRegistered = false;

	// AND Operator (&& - Double Ampersand)
	/* Same concept with multiplication * 0 = 0
		1 * 0 = 0
		1 * 1 = 1
		0 * 1 = 0
		0 * 0 = 0
	 */
	// dapat lahat true para trye lumabas
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator: " + allRequirementsMet);

	// OR Operator (|| = Double Pipe/Bar)
	// Returns true is one of the operands are true
	/* Same concept with addition * 0 = 0
		1 + 0 = 1
		1 + 1 = 1
		0 + 1 = 1
		0 + 0 = 0
	 */
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator: " + someRequirementsMet);


	// NOT Operator (! Exclamation Point)
	// Returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " + someRequirementsNotMet);

